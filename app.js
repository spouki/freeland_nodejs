var createError = require('http-errors');
var express = require('express');
var cors = require('cors');

var path = require('path');

var cookieParser = require('cookie-parser');

var logger = require('morgan');

var bodyParser = require('body-parser');
var multer = require("multer");

var server = require('http').Server(express);
var io = require('socket.io')(server, { origins: '*:*'});
server.listen(420);

io.sockets.on('connection', function (socket) {
  socket.emit('message', { content: 'Vous êtes bien connecté !', importance: '1' });

  socket.on('message', function (message) {
      console.log('Un client me parle ! Il me dit : ' + message);
      socket.broadcast.emit('message', 'Message à toutes les unités. Je répète, message à toutes les unités.');
  });
});

// io.on('connection', function (socket) {
//   console.log('Got a connection over socket io');
//   socket.emit('message', { content: 'Vous êtes bien connecté !', importance: '1' });
// });

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var adminRouter = require('./routes/admin');
var authRouter = require('./routes/auth');
var gamesRouter = require('./routes/games.js');
var messagesRouter = require('./routes/messages.js');

var accesstoken_service_model = require('./models/service/accesstokenServiceModel.js');

var data_model_manager = require('./models/data_model_manager.js');
var users_model = require('./models/users.js');
var users_data_model = require('./models/data/usersDataModel.js');
var games_data_model = require('./models/data/gamesDataModel.js');
var messages_data_model = require('./models/data/messagesDataModel.js');


var app = express();

app.use(cors());

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(function (req, res, next) {
  console.log('Time:', Date.now());
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/admin', adminRouter);
app.use('/auth', authRouter);
app.use('/games', gamesRouter);
app.use('/messages',messagesRouter);

// u = new users_model();
db = new data_model_manager();
// u = new users_data_model();
g = new games_data_model();
m = new messages_data_model();


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // next(createError(404));
  res.status(404).json({"error":true, "message":"API not found."});
  // next();
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({"error":true,
  "message":"Backend failed. See content for detailed report",
  "content":{"locals":res.locals, "stack":res.locals.error.stack}});
  // res.render('error');
});

app.use(function(req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

// io.on('connection', function (socket) {
//   socket.emit('message', { content: 'Vous êtes bien connecté !', importance: '1' });
// });
// socket.on('my other event', function (data) {
//   socket.emit('message', { content: 'Vous êtes bien connecté !', importance: '1' });
// });

module.exports = app;
