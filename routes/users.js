var express = require('express');
var router = express.Router();
// var users_model = "../models/users";


//MIDDLEWARES
router.use(function(req, res, next) {
  let access_token = req.get('access_token');
  console.log('access_token : ' + access_token);
  let user = undefined;
  if (access_token == "admin_token") {
    let options = {"module":"users", "operation":"select", "clauses":[{"field":"privileges", "operator":"=", "comp":1}]};
    user = db.query(options);
  } else {
    let options = {"module":"users", "operation":"select", "clauses":[{"field":"access_token", "operator":"=", "comp":req.get('access_token')}]};
    user = db.query(options);
  }
  // get_by_access_token(access_token);
  // let user = u.get_by_access_token(access_token);
  if (user != null) {
    console.log('Checking  :' + req.originalUrl);
    next();
  } else {
    if (req.originalUrl == "/user" && req.method == "POST") {
      next();
    } else {
      res.status(401).json({"message":"Bad access_token"});
    }
  }
});
//

// ROUTES
router.get('/id/:id?', function(req, res, next) {
  console.log(req.route);
  let options = {"module":"users", "operation":"select", "clauses":[{"field":"id", "operator":"=", "comp":req.params.id}]};
  let user = db.query(options);
  if (user == null) {
    // Error
    res.status(300).json({"error":true, "message":"No user found id ("+req.params.id+").", "code":1});
  } else {
    res.status(200).json(user);
  }
  // res.status(200).json(u.get(req.params.id));
});

router.get('/access_token', function(req, res, next) {
  let access_token = req.get('access_token');
  if (access_token == undefined) {
    console.error('WARNING');
  }
  let options = {
    "module":"users",
     "operation":"select",
     "clauses":[
       {
         "field":"access_token", "operator":"=", "comp":access_token
       }
     ]
   };

  let user = db.query(options);

  user.verify_token(access_token);

  res.status(200).json(user.access_token);
  // res.json({'message':'Hello'});
});

router.post('/user', function(req, res, next) {
  // console.log(req.body);
  // res.json(req.body);
  if (req.body.test == undefined) {
    console.error('Error parameters, review code');
  }
  let options = {"module":"users", "operation":"create", "clauses":{"account":req.body.user}};
  let user = db.query(options);
  if (user == null) {
    // Error
    res.status(300).json({"error":true, "message":"No user found id ("+req.params.id+").", "code":1});
  } else if (user.error == true) {
    res.status(300).json(user);
  }
  res.status(200).json(user);
});
//

module.exports = router;
