var express = require('express');
var router = express.Router();

// MIDDLEWARES
router.use(function(req, res, next) {

  // Checking if wanting to log in
  console.log("route " + req.originalUrl);
  if (req.originalUrl == "/auth/login") {
      console.log("login");
    return next();
  }
  if (req.originalUrl == "/auth/refresh") {
    console.log("Route for refreshing credentials");
    if (req.get('refresh_token') != undefined) {
      console.log("Calling /auth/refresh after middleware pass");
      return next();
    } else {
      return res.status(301).json({"message":"Need refresh token."});
    }
  }

  if (req.originalUrl == "/auth/logout") {
    console.log("Route for terminating session.");
    return next();
  }

  // Getting POST access_token
  let access_token = req.get('access_token');
  console.log('access_token : ' + accesss_token);
  // Now accessing access_token checking in
  let user = u.get_by_access_token(access_token);
  if (user != null) {
    console.log('Checking  :' + req.originalUrl);
    return next();
  }

  res.json({"message":"No user found. Try login in again."})
});
//

// ROUTES
router.post('/login', function(req, res, next) {
  // TODO : check user's privileges in DB
  // let form = JSON.parse(req.body);
  let user = undefined;
  let login_session = undefined;
  if (req.body.username != undefined && req.body.password != undefined) {
    // Need to secure the read of the body
    console.log('username = ' + req.body.username);
    let options = {"module":"users", "operation":"select", "clauses":
    [
      {"field":"username", "operator":"=", "comp":req.body.username},
      {"field":"password", "operator":"=", "comp":req.body.password}
    ]};
    user = db.query(options);
    console.log('User => ' + JSON.stringify(user));
    if (user.ping()==true) {
      user.login(req.body.username, req.body.password);
      login_session = user.access_token;
      console.log('Login ? => ' + JSON.stringify(login_session));
    }
    console.log('User => ' + JSON.stringify(user));
    // user = u.login(req.body.username, req.body.password);
  }
  if (user != null) {
    res.status(200).json(login_session);
  } else {
    res.status(404).json({"message":"No user found."});
  }
});

// Refreshing user session
router.post('/refresh', function(req, res, next) {
  console.log("Refreshing user session : " + req.get('refresh_token'));
  let user = u.get_by_refresh_token(req.get('refresh_token'));
  if (user == null) {
    res.json({"message":"Please use login to initialize credentials."});
  } else {
    user.refresh_credentials();
    res.json(user);
  }
});

router.post('/logout', function(req, res, next) {
  if (req.get('access_token') == undefined) {
    // No access token corresponding
    res.json({"message":"User disconnected."});
  } else {
    if (user.logout() == false) {
      res.json({"message":"Can't disconnect user."})
    } else {
      res.json({"message":"User disconnected."});
    }
  }
});
//

module.exports = router;
