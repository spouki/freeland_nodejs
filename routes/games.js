var express = require('express');
var router = express.Router();
// var users_model = "../models/users";

//MIDDLEWARES
router.use(function(req, res, next) {
  let access_token = req.get('access_token');
  console.log('access_token : ' + access_token);
  if (access_token == "admin_token") {
    let options = {"module":"users", "operation":"select", "clauses":
    [
      {"field":"privileges", "operator":"=", "comp":1}
    ]};
    user = db.query(options);
  } else {
    let options = {"module":"users", "operation":"select", "clauses":
    [
      {"field":"access_token", "operator":"=", "comp":access_token}
    ]};
    user = db.query(options);
  }

  // let user = u.get_by_access_token(access_token);
  if (user != null) {
    console.log('Checking  :' + req.originalUrl);
    res.locals.user_session = user;
    next();
  } else {
    res.status(401).json({"message":"Bad access_token"});
  }
});
//

// ROUTES
router.get('/game/:id?', function(req, res, next) {
  console.log('In /games/game/:id, user session = ' + JSON.stringify(res.locals.user_session));
  console.log(req.route);
  let id = req.params.id;
  if (id === undefined) {
    console.error('An undefined id in games/game/:id');
    id = 0;
  }
  let game = g.get(id);
  res.status(200).json(game);
});

router.get('/', function(req, res, next) {
  g.setUser(res.locals.user_session);
  let games = g.get();
  res.status(200).json(games);
  // res.json({'message':'Hello'});
});

// router.post('/', function(req, res, next) {
//   // console.log(req.body);
//   // res.json(req.body);
//   res.status(200).json();
// });
//

module.exports = router;
