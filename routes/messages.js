var express = require('express');
var router = express.Router();
// var users_model = "../models/users";

//MIDDLEWARES
router.use(function(req, res, next) {
  let access_token = req.get('access_token');
  console.log('access_token : ' + access_token);
  let user = u.get_by_access_token(access_token);
  if (user != null) {
    console.log('Checking  :' + req.originalUrl);
    res.locals.user_session = user;
    next();
  } else {
    res.status(401).json({"message":"Bad access_token"});
  }
});
//

// ROUTES
router.get('/message/:id?', function(req, res, next) {
  console.log('In /messages/message/:id, user session = ' + JSON.stringify(res.locals.user_session));
  console.log(req.route);
  let id = req.params.id;
  if (id == null) {
    console.error('An undefined id in messages/message/:id');
    id = 0;
  }
  let message = m.get(id);
  res.status(200).json(message);
});

router.get('/', function(req, res, next) {
  let messages = m.get();
  res.status(200).json(messages);
  // res.json({'message':'Hello'});
});

// router.post('/', function(req, res, next) {
//   // console.log(req.body);
//   // res.json(req.body);
//   res.status(200).json();
// });
//

module.exports = router;
