var express = require('express');
var router = express.Router();

// MIDDLEWARES
router.use(function(req, res, next) {
  let access_token = req.get('access_token');
  console.log('access_token : ' + access_token);
  let options = {"module":"users", "operation":"select", "clauses":
  [
    {"field":"username", "operator":"=", "comp":req.body.username},
    {"field":"password", "operator":"=", "comp":req.body.password}
  ]};

  // let user = u.get_by_access_token(access_token);
  if (user != null) {
    console.log('Checking  :' + req.originalUrl);
    next();
  } else {
    res.json({"message":"Bad access_token"});
  }
});
//

// ROUTES
router.get('/users', function(req, res, next) {
  res.json(u.get());
});
router.get('/users/:id', function(req, res, next) {
  res.json(u.get(req.params.id));
});
router.post('/users/:id/level/:level', function(req, res, next) {
  user = u.get(req.params.id);
  user.set_level(req.params.level);
  res.json(user);
});
router.post('/users/:id/add_xp/:xp', function(req, res, next) {
  user = u.get(req.params.id);
  user.add_xp(req.params.xp);
  res.json(user);
});
//

module.exports = router;
