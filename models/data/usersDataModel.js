// use User from './user.js';

var user_data_model = require("./userDataModel.js");

class users_data_model {

  constructor() {
    this.users = [];
    // var fs = require("fs");
    // console.log("\n *START* \n");
    // var content = fs.readFileSync("models/data/usersDataModel.json", 'utf8');
    // console.log("Output Content : \n"+ content + ' // TYPE = ' + typeof(content));
    // console.log("\n *EXIT* \n");
    // content = JSON.parse(content);
    let content = [];
    let adminModel = {
      id:0,
      username:"admin",
      password:"pass",
      level:0,
      xp:0,
      privileges:1,
      access_token:undefined,
      games:[],
      // Unused
      online:false,
      last_timestamp:+Date.now() - 360,
      // end Unused
      register_timestamp:1544880037
    };
    content.push(new user_data_model(adminModel));
    for (let ctr = 1; ctr < 2; ctr++) {
      console.log('ctr => ' + ctr);
      let dataModel = {
        id:ctr,
        username:"auto-"+ctr,
        password:"auto-"+ctr,
        level:0,
        xp:0,
        privileges:0,
        access_token:undefined,
        games:[],
        // Unused
        online:false,
        last_timestamp:+Date.now() - 360,
        // end Unused
        register_timestamp:1544880037
      };
      content.push(new user_data_model(dataModel));
    }

    for (var id in content) {
      console.log(content[id]);
      this.users.push(new user_data_model(content[id]));
    }
  }

  get(id = null) {
    console.log("...Get ID = " + id);
    // TODO : Check if id in user list
    if (id == null) {
      let users = [];
      let users_loop = this.users;
      for (var user of users_loop) {
        // if (user.owner != res.locals.user_session.id || res.locals.user_session.privileges != true) {
          //
        if (user.compareOwner(this.user.id) == true) {
          console.log('Pushing : ' + JSON.stringify(user));
          users.push(user);
        } else {
        }
      }
      return(users);
    }
    this.user = this.users[id];
    if (user.owner != this.user.id) {
      //
      user = null;
    }
    return(user);
  }

  setUser(user) {
    this.user = user;
  }

  get_by_access_token(access_token = null) {
    if (access_token == null) {
      return null;
    }
    if (access_token === "admin_token") {
      return this.users[0];
    }
    console.log('Checking');
    for (let user of this.users) {
      if (user == null) {
        return null;
      }
      console.log('Checking user : ' + JSON.stringify(user));
      if (user.verify_token(access_token) == true) {
        return user;
      }
    }
    return null;
  }

  create(newuser) {
    console.log('NEW USER => ' + JSON.stringify(newuser));
    let user_pre_account = newuser.account;
    // TODO : Should verify param integrity before checking if user already exists

    // Checking if user already exists
    if (this.exists(user_pre_account) == true) {
      // Error user already exists
      return {"error":true, "message":"user already exists", "code":"1"};
    }
    let new_user_data_model = new user_data_model(user_pre_account);
    if (new_user_data_model == null) {
      return {"error":true, "message":"Cant create user account.", "code":1};
    }
    console.log('Created new user : ' + JSON.stringify(new_user_data_model));
    this.users.push(new_user_data_model);
    return {"error":false, "content":new_user_data_model};
  }

  exists(usercheck) {
    for (let idx in this.users) {
      if (this.users[idx]["username"] == usercheck.username) {
        return true;
      }
    }
    return false;
  }

  select(params) {

    for (var idx = 0; idx < this.users.length; idx++) {
      // field ('id'), operator ('='), comp ('idvalue')
      console.log('Checking select params ('+params[0]["field"]+' => '+params[0]["comp"]+') : '
      + JSON.stringify(this.users[idx]));
      console.log('test');
      console.log('User\'s field ('+params[0]["field"]+') = ('+this.users[idx][params[0]["field"]]+')')

      if (params[0].operator === "=") {
        console.log("Equal operator");

        let used = 0;
        let total = 0;
        for (let idx_params in params) {
          console.log('idx_p => ' + JSON.stringify(idx_params));
          console.log(this.users[idx][params[idx_params]["field"]]);
          if (this.users[idx][params[idx_params]["field"]] == params[idx_params]["comp"]) {
            // Setting that every parameter is used to compare/filter
            used++;
          }
          total = idx_params;
        }
        console.log('Checked ' + used + ' for ' + total + ' in total. Length ' + params.length);
        // then

        if (used == params.length) {
          console.log('Returning => ' + JSON.stringify(this.users[idx]));
          let user = new user_data_model(this.users[idx]);
          console.log('====> Returning => ' + JSON.stringify(user));
          if (user.ping() == true) {
            console.log('user module successfuly loaded');
          }
          return user;
        }

      }
    }
    console.log('Length of users list => ' + this.users.length);
    return null;
  }

}

module.exports = users_data_model;
