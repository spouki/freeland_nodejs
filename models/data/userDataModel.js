

class user_data_model {

  constructor(dataModel) {
    this.id = dataModel['id'];
    this.username = dataModel['username'];
    this.password = dataModel['password'];
    this.level = dataModel['level'];
    this.xp = dataModel['xp'];
    this.privileges = dataModel['privileges'];
    if (dataModel['access_token'] != undefined) {
      console.log('Access_token : ' + JSON.stringify(dataModel['access_token']));
      this.access_token = dataModel['access_token'];
    } else {
      this.access_token = undefined;
    }
    this.games = dataModel['games_json'];
    this.online = false;
    this.last_timestamp = +Date.now() - 360;
    this.register_timestamp = dataModel['timestamp'];
    console.log('user )> ' + JSON.stringify(this));
  }

  compareOwner(id) {
    console.log('ID =>>>>' + id);
    return this.owner === id;
  }
  get() {
    if (this.last_timestamp < +Date.now() - 360 && this.online == true) {
      this.online = false;
    }
    return(this);
  }

  // Attributes member functions related
  set_level(level) {
    this.level = level;
  }
  get_level() {
    return this.level;
  }

  add_xp(xp) {
    this.xp = parseInt(this.xp) +  parseInt(xp);
    while (this.xp > xp_array[this.level]) {
      this.level += 1;
    }
  }
  get_xp() {
    return parseInt(this.xp);
  }
  //

  logout(access_token) {
    if (this.access_token.verify_token(access_token) == true) {
      this.access_token.delete_access(access_token);
      return true;
    }
    return false;
  }

  // Login and auth
  login(username, password) {
    // Need to check validity of params
    console.log('Login in ['+username+','+password+']');
    console.log('DEBUG = ' + JSON.stringify(username));
    if (username == this.username && password == this.password) {
      console.log('Good user');
      // Check access_token
      // this.access_token = new access_token();
      //accesstoken_service_model
      this.access_token = db.accesstoken_service_model.refresh();
      console.log('service Access token => ' + JSON.stringify(this.access_token));
      return this.access_token;
    }
    console.log('Nope');
    return null;
  }

  // Before using user authenticated function
  verify_token(access_token) {
    console.log("In user verify_token");
    if (access_token != null && this.access_token != undefined) {
      console.log("Checking in accesss token");
      return (this.access_token.verify_token(access_token));
    }
    console.log("Error, accesss_token got : " + access_token + " access_token in this : " + this.accesss_token);
    return false;
  }
  //
  verify_refresh_token(refresh_token) {
    if (this.access_token == undefined) {
      console.log("Bad refresh token");
      return false;
    }
    console.log("In user verify_refresh_token : " + refresh_token);
    console.log("In user object refresh_token : " + this.access_token.get_refresh_token());
    if (refresh_token != null && this.access_token != undefined) {
      console.log("Checking in refresh token");
      return (this.access_token.verify_refresh_token(refresh_token));
    }

    return false;
  }

  refresh_credentials() {
    this.access_token.refresh();
    return (this.access_token.toJson());
  }
  //

  ping() {
    console.log('PING');
    return true;
  }

};

module.exports = user_data_model;
