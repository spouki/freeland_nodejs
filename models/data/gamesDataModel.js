// use User from './user.js';

var game_data_model = require("./gameDataModel.js");

class games_data_model {

  constructor() {
    this.games = [];
    var fs = require("fs");
    console.log("\n *START* \n");
    var content = fs.readFileSync("models/data/gamesDataModel.json", 'utf8');
    console.log("Output Content : \n"+ content + ' // TYPE = ' + typeof(content));
    console.log("\n *EXIT* \n");
    content = JSON.parse(content);
    for (var id in content) {
      console.log(content[id]);
      this.games.push(new game_data_model(content[id]));
    }
  }

  get(id = null) {
    console.log("...Get ID = " + id);
    // TODO : Check if id in user list
    if (id == null) {
      let games = [];
      let games_loop = this.games;
      for (var game of games_loop) {
        // if (game.owner != res.locals.user_session.id || res.locals.user_session.privileges != true) {
          //
        if (game.compareOwner(this.user.id) == true) {
          console.log('Pushing : ' + JSON.stringify(game));
          games.push(game);
        } else {
        }
      }
      return(games);
    }
    game = this.games[id];
    if (game.owner != this.user.id) {
      //
      game = null;
    }
    return(game);
  }

  setUser(user) {
    this.user = user;
  }



}

module.exports = games_data_model;
