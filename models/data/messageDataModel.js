

class message_data_model {

  constructor(dataModel) {
    this.id = dataModel['id'];
    this.content = dataModel['content'];
    this.timestamp = dataModel['timestamp'];
    this.sender = dataModel['sender'];
    this.dest = dataModel['dest'];
    console.log('message )> ' + JSON.stringify(this));
  }

  get() {
    return(this);
  }

  compareOwner(id) {
    console.log('ID =>>>>' + id);
    return this.owner === id;
  }
};

module.exports = message_data_model;
