

class game_data_model {

  constructor(dataModel) {
    this.id = dataModel['id'];
    this.name = dataModel['name'];
    this.owner = dataModel['owner'];
    this.timestamp = dataModel['timestamp'];
    console.log('GAME )> ' + JSON.stringify(this));
  }

  get() {
    return(this);
  }

  compareOwner(id) {
    console.log('ID =>>>>' + id);
    return this.owner === id;
  }
};

module.exports = game_data_model;
