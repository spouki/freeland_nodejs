// use User from './user.js';

var message_data_model = require("./messageDataModel.js");

class messages_data_model {

  constructor() {
    this.messages = [];
    var fs = require("fs");
    console.log("\n *START* \n");
    var content = fs.readFileSync("models/data/messagesDataModel.json", 'utf8');
    console.log("Output Content : \n"+ content + ' // TYPE = ' + typeof(content));
    console.log("\n *EXIT* \n");
    content = JSON.parse(content);
    for (var id in content) {
      console.log(content[id]);
      this.messages.push(new message_data_model(content[id]));
    }
  }

  get(id = null) {
    console.log("...Get ID = " + id);
    // TODO : Check if id in user list
    if (id == null) {
      let messages = [];
      let messages_loop = this.messages;
      for (var message of messages_loop) {
        // if (game.owner != res.locals.user_session.id || res.locals.user_session.privileges != true) {
          //
        if (message.compareOwner(this.user.id) == true) {
          console.log('Pushing : ' + JSON.stringify(message));
          messages.push(message);
        } else {
        }
      }
      return(messages);
    }
    message = this.messages[id];
    if (game.owner != this.user.id) {
      //
      message = null;
    }
    return(message);
  }

  setUser(user) {
    this.user = user;
  }



}

module.exports = messages_data_model;
