const crypto = require('crypto');

class access_token {
  constructor() {
    this.access_token = new crypto.randomBytes(20).toString('hex');
    this.expire_time = 360;
    this.timestamp = Math.round(+new Date()/1000);
    this.refresh_token = new crypto.randomBytes(20).toString('hex');
  }

  refresh() {
    this.access_token = new crypto.randomBytes(20).toString('hex');;
    this.expire_time = 360;
    this.timestamp = Math.round(+new Date()/1000);
    this.refresh_token = new crypto.randomBytes(20).toString('hex');
    return(this.toJson());
  }

  check_freshness() {
    let current_timestamp = Math.round(+new Date()/1000);
    if (current_timestamp < this.timestamp + this.expire_time) {
      // Should have the access_token
      return true;
    } else if (current_timestamp >= this.timestamp + this.expire_time) {
      // Should refresh the access_token
      return false;
    }
  }

  verify_token(access_token) {
    console.log('access_token received : ' + access_token + " In store : " + this.access_token);
    if (this.access_token == access_token) {
      return true;
    }
    return false;
  }

  verify_refresh_token(refresh_token) {
    if (this.refresh_token == refresh_token) {
      return true;
    }
    return false;
  }

  get_refresh_token() {
    return this.refresh_token;
  }

  delete_access() {
    this.access_token = undefined;
    this.expire_time = undefined;
    this.timestamp = undefined;
    this.refresh_token = undefined;
    return true;
  }

  toJson() {
    return (this);
  }

};

module.exports = access_token;
