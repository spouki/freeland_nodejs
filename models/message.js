
var messages_json = [{
  "id":0,
  "name":"Test message",
  "Requests":{
    "ping":"/message/"+"0"+"/ping",
    "run":"/message/"+"0"+"/run"
  }
}];

class message_model {
  constructor(id, content, sender, dest) {
    this.id = id;
    this.content = content;
    this.timestamp = +Date.now()
    this.sender = sender;
    this.dest = dest;
  }

  get() {
    return(this);
  }

};

module.exports = message_model;
