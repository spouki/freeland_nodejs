var message_model = require("./message.js");

class messages_model {

  constructor() {
    this.messages = [];

    this.messages.push(new message_model(0, 'Test message 0', 0, 1));
    this.messages.push(new message_model(1, 'Test message 1', 1, 0));
  }

  get(id = null) {
    console.log("Get ID = " + id);
    // TODO : Check if id in user list
    if (id == null) {
      let messages = this.messages;
      return(this.messages);
    }
    if (this.messages[id].sender != res.locals.user_session.username && this.messages[id].dest != res.locals.user_session.username && res.locals.user_session.privileges != true) {
      return null;
    }
    return(this.messages[id]);
  }
}

module.exports = messages_model;
