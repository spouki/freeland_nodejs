var access_token = require("./access_token.js");

var xp_array = [
  0,
  100,
  200,
  400,
  800,
  1600,
  3200
];

var games_json = [{
  "id":0,
  "name":"Test",
  "Requests":{
    "ping":"/games/"+"0"+"/ping",
    "run":"/games/"+"0"+"/run"
  }
}];

class user_model {
  constructor(id, username, password, level, xp, privileges) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.level = level;
    this.xp = xp;
    this.privileges = privileges;
    this.access_token = undefined;
    this.games = games_json;
    this.online = false;
    this.last_timestamp = +Date.now() - 360;
  }

  get() {
    if (this.last_timestamp < +Date.now() - 360 && this.online == true) {
      this.online = false;
    }
    return(this);
  }

  // Attributes member functions related
  set_level(level) {
    this.level = level;
  }
  get_level() {
    return this.level;
  }

  add_xp(xp) {
    this.xp = parseInt(this.xp) +  parseInt(xp);
    while (this.xp > xp_array[this.level]) {
      this.level += 1;
    }
  }
  get_xp() {
    return parseInt(this.xp);
  }
  //

  logout(access_token) {
    if (this.access_token.verify_token(access_token) == true) {
      this.access_token.delete_access(access_token);
      return true;
    }
    return false;
  }

  // Login and auth
  login(username, password) {
    // Need to check validity of params
    if (username == this.username && password == this.password) {
      // Check access_token
      this.access_token = new access_token();
    }
  }

  // Before using user authenticated function
  verify_token(access_token) {
    console.log("In user verify_token");
    if (access_token != null && this.access_token != undefined) {
      console.log("Checking in accesss token");
      return (this.access_token.verify_token(access_token));
    }
    console.log("Error, accesss_token got : " + access_token + " access_token in this : " + this.accesss_token);
    return false;
  }
  //
  verify_refresh_token(refresh_token) {
    if (this.access_token == undefined) {
      console.log("Bad refresh token");
      return false;
    }
    console.log("In user verify_refresh_token : " + refresh_token);
    console.log("In user object refresh_token : " + this.access_token.get_refresh_token());
    if (refresh_token != null && this.access_token != undefined) {
      console.log("Checking in refresh token");
      return (this.access_token.verify_refresh_token(refresh_token));
    }

    return false;
  }

  refresh_credentials() {
    this.access_token.refresh();
    return (this.access_token.toJson());
  }
  //

  toJson() {
    return (JSON.stringify(this));
  }

};

module.exports = user_model;
