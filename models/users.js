// use User from './user.js';

var user_model = require("./user.js");

class users_model {

  constructor() {
    this.users = [];

    this.users.push(new user_model(0, "admin", "pass", 1, 150, true));
    this.users.push(new user_model(0, "user", "pass", 0, 0, false));
  }

  get(id = null) {
    console.log("Get ID = " + id);
    // TODO : Check if id in user list
    if (id == null) {
      return(this.users);
    }
    return(this.users[id]);
  }

  get_by_username(username = null) {
    if (username == null) {
      return null
      // return (JSON.stringify({"message":"No username received."}))
    }
    for (let user of this.users) {
      if (user.username == username) {
        return user;
      }
    }
    return null
    // return (JSON.stringify({"message":"No user with name : " + username}));
  }

  get_by_access_token(access_token = null) {
    if (access_token == null) {
      return null;
    }
    if (access_token === "admin_token") {
      return this.users[0];
    }
    console.log('Checking');
    for (let user of this.users) {
      if (user == null) {
        return null;
      }
      console.log('Checking user : ' + JSON.stringify(user));
      if (user.verify_token(access_token) == true) {
        return user;
      }
    }
    return null;
  }

  get_by_refresh_token(refresh_token = null) {
    if (refresh_token == null) {
      return null;
    }
    console.log('Refreshing token');
    for (let user of this.users) {
      if (user == null) {
        return null;
      }
      console.log("User => " + JSON.stringify(user));
      if (user.verify_refresh_token(refresh_token) == true) {
        console.log("Found user");
        user.refresh_credentials()
        return user;
      } else {
        return null;
      }
    }
  }

  post() {
    console.log("Pushing");
    this.users.push(new user_model(this.users.length));
    return this.get(this.users.length-1);
  }

  login(username, password) {
    // Find user by username
    let user = this.get_by_username(username);
    if (user == null) {
      return null
    }
    user.login(username, password);
    return user.access_token;
  }


}

module.exports = users_model;
