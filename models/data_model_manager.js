var users_data_model = require('./data/usersDataModel.js');
var games_data_model = require('./data/gamesDataModel.js');
var accesstoken_service_model = require('./service/accesstokenServiceModel');


class data_model_manager {
  constructor() {
    console.log('Data Model Manager created');
    this.accesstoken_service_model = new accesstoken_service_model();
    this.users_module = new users_data_model();
  }

  query(query_params) {
    console.log('Query params : ' + JSON.stringify(query_params));
    if (query_params == null) {
      return {"error":true, "message":"no parameters to query DB to.", "code":1};
    }
    var module = undefined;
    switch (query_params.module) {
      case "users":
      console.log('Users query params, so should return a call to users module filtered by options');
      module = this.users_module;
      break;
      case "games":
      console.log('Games query params, so should return a call to users module filtered by options');
      break;
      default:
      console.log('Default query params, so might be an error');
      return null;
    }

    var tt = {
      test:() => {
        console.log('Ok');
      }
    };
    tt.test();
    var op = query_params.operation;
    console.log('OP => ' + op);
    switch (op) {
      case 'select':
      console.log('select = good');
      console.log("Query params clauses => " + JSON.stringify(query_params.clauses));
      // let mod = module.op(query_params.clauses);
      // console.log('Module users : ' + JSON.stringify(mod));
      return module.select(query_params.clauses);
      break;
      case 'create':
      console.log('create = good');
      console.log("Query params clauses => " + JSON.stringify(query_params.clauses));
      return module.create(query_params.clauses);
      break;
      default:
      console.log('default, not good');
    }
    // module.select(query_params.clauses);
    // module.op(query_params.clauses);
    //

    // At that moment, only need to return null;
    return null;

  }
}

module.exports = data_model_manager;
